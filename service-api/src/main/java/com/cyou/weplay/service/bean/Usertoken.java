package com.cyou.weplay.service.bean;

/**
 * Usertoken
 *
 * @author zhaomingyu
 */
public class Usertoken {

    private long uid;

    private String udid;

    private String utoken;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public String getUtoken() {
        return utoken;
    }

    public void setUtoken(String utoken) {
        this.utoken = utoken;
    }

}
