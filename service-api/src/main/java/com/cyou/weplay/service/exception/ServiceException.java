package com.cyou.weplay.service.exception;

/**
 * ServiceException
 *
 * @author zhaomingyu
 */
public class ServiceException extends Exception {

    protected String errorcode;

    public ServiceException(String errorcode) {
        this.errorcode = errorcode;
    }

    public String getErrorcode() {
        return this.errorcode;
    }

}
