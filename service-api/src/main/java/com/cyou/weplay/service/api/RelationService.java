package com.cyou.weplay.service.api;

import com.cyou.weplay.service.bean.UserBean;

import java.util.List;

/**
 * RelationService
 *
 * @author zhaomingyu
 */
public interface RelationService extends RemoteService {

    /**
     * 关注用户
     * @param uid
     * @param followingUid 
     */
    void followUser(long uid, long followingUid) throws Exception;

    /**
     * 取消关注
     * @param uid
     * @param followingUid 
     */
    void unfollowUser(long uid, long followingUid) throws Exception;
    
    /**
     * 
     * @param myuid
     * @param otheruid
     * @return
     * @throws Exception
     */
    public boolean isFocusUser(long myuid, long otheruid) throws Exception;
    
    /**
     * 
     * @param myuid
     * @param otheruid
     * @return
     * @throws Exception
     */
    public boolean isFansUser(long myuid, long otheruid) throws Exception;

    /**
     * 获取关注列表
     * @param uid
     * @return 
     */
    List<UserBean> getUserFollowings(long uid);

    /**
     * 获取粉丝列表
     * @param uid
     * @return 
     */
    List<UserBean> getUserFollowers(long uid);
}
