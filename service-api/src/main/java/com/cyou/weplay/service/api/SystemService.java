package com.cyou.weplay.service.api;

import com.cyou.weplay.service.exception.ServiceException;

/**
 * SystemService
 *
 * @author zhaomingyu
 */
public interface SystemService {

    long incrAndGet(String counterName) throws ServiceException;

    String getGlobalVar(String varname);
}
