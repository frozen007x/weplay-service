package com.cyou.weplay.service.api;

import com.cyou.weplay.service.bean.UserBean;

/**
 * AccountService
 *
 * @author zhaomingyu
 */
public interface AccountService extends RemoteService {

    /**
     * 根据加密信息得出的udid匹配用户
     *
     * @param secInfo 加密信息
     * @return UserBean
     * @throws java.lang.Exception
     */
    UserBean matchUser(String secInfo) throws Exception;

    /**
     * 获取用户认证信息
     * @param uid
     * @return new String[] {uauth, token}
     * @throws Exception 
     */
    String[] getUserAuth(long uid) throws Exception;

    /**
     * 用户验证
     *
     * @param uauth
     * @param token
     * @return uid
     * @throws Exception
     */
    long authUser(String uauth, String token) throws Exception;

}
