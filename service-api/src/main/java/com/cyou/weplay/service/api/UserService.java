package com.cyou.weplay.service.api;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.cyou.weplay.service.bean.UserBean;

/**
 * UserService
 *
 * @author zhaomingyu
 */
public interface UserService extends RemoteService {

    /**
     * 获取用户对象
     *
     * @param uid 用户id
     * @return {@link UserBean}
     * @throws Exception
     */
    UserBean getUserBean(long uid) throws Exception;

    /**
     * 创建初始用户属性
     *
     * @param uid 用户id
     * @param nickname 昵称
     * @param status 状态
     * @param udid 设备标识
     * @param imei IMEI
     * @param imsi IMSI
     * @param sysid 系统标识
     * @return UserBean
     * @throws Exception
     */
    UserBean createUserBean(long uid, String nickname, int status, String udid, String imei, String imsi, String sysid) throws Exception;

    /**
     * 获取用户对象列表
     *
     * @param uids 用户id集合
     * @return List<{@link UserBean}>
     * @throws java.lang.Exception
     */
    List<UserBean> getUserBeans(Collection<Long> uids) throws Exception;

    /**
     * 获取用户属性
     *
     * @param uid
     * @param fields
     * @return String[] 用户属性数组
     * @throws java.lang.Exception
     */
    String[] getUserBeanFields(long uid, String... fields) throws Exception;

    /**
     * 批量获取指定用户id的属性
     *
     * @param uids 用户id集合
     * @param fields 字段名称数组
     * @return Map<uid, field array>
     * @throws java.lang.Exception
     */
    Map<Long, String[]> getFieldsOfUsers(Collection<Long> uids, String... fields) throws Exception;

}
