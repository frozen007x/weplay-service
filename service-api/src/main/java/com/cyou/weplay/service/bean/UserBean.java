package com.cyou.weplay.service.bean;

import java.io.Serializable;

/**
 * UserBean
 *
 * @author zhaomingyu
 */
public class UserBean implements Serializable {

    @FieldMapping(mandatory = true)
    private long uid;

    @FieldMapping
    private String nickname;

    @FieldMapping
    private String avatar;

    @FieldMapping
    private String avatarM;

    @FieldMapping
    private String avatarS;

    @FieldMapping
    private String avatarBg;

    @FieldMapping
    private int gender = 1;

    @FieldMapping
    private String signature;

    @FieldMapping
    private String areaid;

    @FieldMapping
    private String favgame;

    @FieldMapping
    private int status;

    @FieldMapping
    private String udid;

    @FieldMapping
    private String imei;

    @FieldMapping
    private String imsi;

    @FieldMapping
    private String sysid;

    @FieldMapping
    private long createdate;

    @FieldMapping
    private String birthdate;

    @FieldMapping
    private String type;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarM() {
        return avatarM;
    }

    public void setAvatarM(String avatarM) {
        this.avatarM = avatarM;
    }

    public String getAvatarS() {
        return avatarS;
    }

    public void setAvatarS(String avatarS) {
        this.avatarS = avatarS;
    }

    public String getAvatarBg() {
        return avatarBg;
    }

    public void setAvatarBg(String avatarBg) {
        this.avatarBg = avatarBg;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getAreaid() {
        return areaid;
    }

    public void setAreaid(String areaid) {
        this.areaid = areaid;
    }

    public String getFavgame() {
        return favgame;
    }

    public void setFavgame(String favgame) {
        this.favgame = favgame;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getSysid() {
        return sysid;
    }

    public void setSysid(String sysid) {
        this.sysid = sysid;
    }

    public long getCreatedate() {
        return createdate;
    }

    public void setCreatedate(long createdate) {
        this.createdate = createdate;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
