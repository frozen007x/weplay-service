package com.cyou.weplay.shard;

import java.util.List;

/**
 * ShardPlan
 *
 * @author zhaomingyu
 * @param <G> ShardGroup
 */
public class ShardPlan<G extends ShardGroup> {

    protected List<G> shardGroupList;

    public List<G> getShardGroupList() {
        return shardGroupList;
    }

    public void setShardGroupList(List<G> shardGroupList) {
        this.shardGroupList = shardGroupList;
    }

}
