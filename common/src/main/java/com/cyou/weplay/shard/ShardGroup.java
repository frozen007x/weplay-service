package com.cyou.weplay.shard;

/**
 * ShardGroup
 * @author zhaomingyu
 */
public class ShardGroup {

    protected String shardKey;

    public String getShardKey() {
        return shardKey;
    }

    public void setShardKey(String shardKey) {
        this.shardKey = shardKey;
    }

}
