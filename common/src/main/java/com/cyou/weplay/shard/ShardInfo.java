package com.cyou.weplay.shard;

/**
 * ShardInfo
 *
 * @author zhaomingyu
 */
public class ShardInfo {

    protected String shardKey;

    public ShardInfo(String shardKey) {
        this.shardKey = shardKey;
    }

    public String getShardKey() {
        return shardKey;
    }

}
