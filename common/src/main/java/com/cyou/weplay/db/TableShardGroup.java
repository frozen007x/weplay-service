package com.cyou.weplay.db;

import javax.sql.DataSource;

import com.cyou.weplay.shard.ShardGroup;

/**
 * TableShardGroup
 *
 * @author zhaomingyu
 */
public class TableShardGroup extends ShardGroup {

    protected DataSource datasource;

    protected String tableName;

    protected int minSeq;

    protected int maxSeq;

    public DataSource getDatasource() {
        return datasource;
    }

    public void setDatasource(DataSource datasource) {
        this.datasource = datasource;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getMinSeq() {
        return minSeq;
    }

    public void setMinSeq(int minSeq) {
        this.minSeq = minSeq;
    }

    public int getMaxSeq() {
        return maxSeq;
    }

    public void setMaxSeq(int maxSeq) {
        this.maxSeq = maxSeq;
    }

}
