package com.cyou.weplay.db;

/**
 * SimpleTableTemplate
 *
 * @author zhaomingyu
 */
public abstract class SimpleTableTemplate extends TableTemplate<Void> {

    public SimpleTableTemplate(TableShardManager shardManager) {
        super(shardManager);
    }

}
