package com.cyou.weplay.db;

import javax.sql.DataSource;

import com.cyou.weplay.shard.ShardInfo;

/**
 * TableShardInfo
 *
 * @author zhaomingyu
 */
public class TableShardInfo extends ShardInfo {

    protected DataSource datasource;

    protected String shardedName;

    public TableShardInfo(String shardKey, DataSource datasource, String shardedName) {
        super(shardKey);
        this.datasource = datasource;
        this.shardedName = shardedName;
    }

    public DataSource getDatasource() {
        return datasource;
    }

    public String getShardedName() {
        return shardedName;
    }

}
