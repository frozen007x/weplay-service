package com.cyou.weplay.db;

/**
 * ShardedJdbcDataLayer
 *
 * @author zhaomingyu
 */
public abstract class ShardedJdbcDataLayer extends JdbcDataLayer {

    protected TableShardManager shardManager;

    public ShardedJdbcDataLayer(TableShardManager tableShardManager) {
        this.shardManager = tableShardManager;
    }

    public void setShardManager(TableShardManager shardManager) {
        this.shardManager = shardManager;
    }
}
