package com.cyou.weplay.db;

import java.sql.Connection;

/**
 * JdbcDataLayer
 * 
 * @author zhaomingyu
 */
public abstract class JdbcDataLayer {

    protected Connection conn = null;

    public void setConnection(Connection conn) {
        this.conn = conn;
    }
}
