package com.cyou.weplay.db;

import com.cyou.weplay.shard.ShardPlan;

/**
 * TableShardPlan
 *
 * @author zhaomingyu
 */
public class TableShardPlan extends ShardPlan<TableShardGroup> {

    String tableName;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

}
