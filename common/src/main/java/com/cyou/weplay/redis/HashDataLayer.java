package com.cyou.weplay.redis;

import java.util.Map;

import com.cyou.weplay.db.TableShardManager;

/**
 * HashDataLoader
 *
 * @author zhaomingyu
 * @param <E>
 */
public abstract class HashDataLayer<E extends RedisEntity> extends RedisDataLayer<Map<String, String>, E> {

    protected HashDataLayer(TableShardManager shardManager) {
        super(shardManager);
    }

}
