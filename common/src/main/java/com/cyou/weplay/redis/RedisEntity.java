package com.cyou.weplay.redis;

import redis.clients.jedis.JedisCommands;

/**
 * RedisEntity
 *
 * @author zhaomingyu
 */
public abstract class RedisEntity {

    protected String entityId;

    protected String cacheKey = null;

    protected int expireSeconds = 0;

    public abstract String getPrefix();

    public String getKey() {
        if (cacheKey == null) {
            cacheKey = getPrefix() + entityId;
        }
        return cacheKey;
    }

    public void resetExpire(String key, JedisCommands jedis) {
        if (expireSeconds > 0) {
            jedis.expire(key, expireSeconds);
        }
    }
}
