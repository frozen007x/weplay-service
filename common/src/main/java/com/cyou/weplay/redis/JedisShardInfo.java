package com.cyou.weplay.redis;

import com.cyou.weplay.shard.ShardInfo;

import redis.clients.jedis.JedisPool;

/**
 * JedisShardInfo
 *
 * @author zhaomingyu
 */
public class JedisShardInfo extends ShardInfo {

    protected JedisPool pool;

    public JedisShardInfo(String shardKey) {
        super(shardKey);
    }

    
}
