package com.cyou.weplay.redis;

import redis.clients.jedis.JedisCommands;
import redis.clients.jedis.ShardedJedis;

/**
 * JedisConnection
 *
 * @author zhaomingyu
 */
public class JedisConnection {

    protected ShardedJedis jedis;

    public JedisConnection(ShardedJedis jedis) {
        this.jedis = jedis;
    }

    public ShardedJedis getJedis() {
        return jedis;
    }

    public JedisCommands getJedisCommands() {
        return jedis;
    }

    public void close() {
        jedis.close();
    }
}
