package com.cyou.weplay.utils;

import java.util.Random;

/**
 * NumUtils
 *
 * @author zhaomingyu
 */
public class NumUtils {
    private static final Random rand = new Random();

    public static int parseInt(String src, int def) {
        if (src == null) {
            return def;
        }
        int res = def;
        try {
            res = Integer.parseInt(src);
        } catch (NumberFormatException e) {
        }
        return res;
    }

    public static int parseInt(Object src, int def) {
        if (src == null) {
            return def;
        }
        return parseInt(src.toString(), def);
    }

    public static long parseLong(String src, long def) {
        if (src == null) {
            return def;
        }
        long res = def;
        try {
            res = Long.parseLong(src);
        } catch (NumberFormatException e) {
        }
        return res;
    }

    public static double parseDouble(Object src, double def) {
        if (src == null) {
            return def;
        }
        return parseDouble(src.toString(), def);
    }

    public static double parseDouble(String src, double def) {
        if (src == null) {
            return def;
        }
        double res = def;
        try {
            res = Double.parseDouble(src);
        } catch (NumberFormatException e) {
        }
        return res;
    }
}
