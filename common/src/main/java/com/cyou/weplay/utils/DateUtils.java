package com.cyou.weplay.utils;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * DateUtils
 * @author zhaomingyu
 */
public class DateUtils {
    private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    public static String getCurrentDateTime() {
        return getDateTime(SysTimeManager.currentTimeMillis());
    }

    public static String getDateTime(long timeMillis) {
        return fmt.print(timeMillis);
    }
}
