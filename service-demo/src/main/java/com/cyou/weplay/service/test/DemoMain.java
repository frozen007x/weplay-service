package com.cyou.weplay.service.test;

import com.cyou.weplay.service.api.UserService;
import com.cyou.weplay.service.bean.UserBean;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * DemoMain
 *
 * @author zhaomingyu
 */
public class DemoMain {
    
    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"applicationContext-demo.xml"});
        context.start();
        UserService userService = (UserService) context.getBean("userService");
        /*
         System.out.println("Beging " + new Date());
         userService.saveUserBean(98, "weplay98", 0, "", "", "", "");
         System.out.println("End " + new Date());
         */
        /*
         for (int i = 1; i <= 100; i++) {
         //userService.saveUserBean(i, "weplay" + i, 0, "udid" + i, "imei" + i, "imsi" + i, "sysid" + i);
         UserBean user = userService.getUserBean(i);
         System.out.println(user.getNickname());
         }*/
        while(true) {
            System.out.println(userService.getUserBean(1));
        }
        //System.out.println(userService.getUserBean(1));
    }
}
