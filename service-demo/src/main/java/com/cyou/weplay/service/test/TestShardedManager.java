package com.cyou.weplay.service.test;

import java.util.Map;

import com.cyou.weplay.cache.layer.UserHashLayer;
import com.cyou.weplay.cache.model.UserHash;
import com.cyou.weplay.db.TableShardManager;
import com.cyou.weplay.service.api.UserService;
import com.cyou.weplay.service.bean.UserBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

/**
 * TestShardedManager
 *
 * @author zhaomingyu
 */
public class TestShardedManager {
    
    private static ApplicationContext context = null;
    
    private static TableShardManager tableShardManager = null;
    
    public static void main(String[] args) throws Exception {
        context = new ClassPathXmlApplicationContext(new String[]{"applicationContext-xa.xml", "applicationContext-beans.xml", "applicationContext-shard.xml"});
        tableShardManager = (TableShardManager) context.getBean("tableShardManager");
        
        //t001();
        //testXA();
    }

    public static void testXA() throws Exception {
        TestServiceImpl testServ = (TestServiceImpl) context.getBean("testService");
        testServ.testXA();
    }
    
    public static void t001() throws Exception {
        UserHashLayer hashlayer = new UserHashLayer(tableShardManager);
        
        for (long i = 1; i <= 200; i++) {
            UserHash userhash = new UserHash(i);
            UserBean ub = userhash.getEntityBean();
            ub.setNickname("weplay" + i);
            ub.setStatus(0);
            ub.setUdid("udid" + i);
            ub.setImei("imei" + i);
            ub.setImsi("imsi" + i);
            ub.setSysid("sysid" + i);
            hashlayer.saveData(userhash);
        }
    }
    
    public static void t002() throws Exception {
        UserHashLayer hashlayer = new UserHashLayer(tableShardManager);
        
        Map<String, String> user900 = hashlayer.getData("99");
        System.out.println(user900.get("nickname"));
    }
    
    public static void t003() throws Exception {
        ShardedJedisPool pool = (ShardedJedisPool) context.getBean("jedisPool");
        ShardedJedis jedis = pool.getResource();
        
        UserHashLayer hashlayer = new UserHashLayer(tableShardManager);
        for (long i = 1; i <= 100; i++) {
            UserHash userhash = new UserHash(i);
            userhash.setHashDataLayer(hashlayer);
            userhash.getAllFields(jedis);
            System.out.println(userhash.getEntityBean().getNickname());
        }
        jedis.close();
        
    }

    public static void t004() throws Exception {
        UserService userService = (UserService) context.getBean("userService");
        
        for (int i = 1; i <= 100; i++) {
            userService.createUserBean(i, "weplay" + i, 0, "udid" + i, "imei" + i, "imsi" + i, "sysid" + i);
            UserBean user = userService.getUserBean(i);
            System.out.println(user.getNickname());
        }
        //userService.saveUserBean(30000, "weplay1001", 0, "", "", "", "");
        /*
        userService.saveUserBean(30000, "weplay30000", 0, "", "", "", "");
        userService.saveUserBean(1, "weplay30000", 0, "", "", "", "");
        */
    }

    public static void t005() throws Exception {
        UserService userService = (UserService) context.getBean("userService");
        UserBean userbean = userService.getUserBean(85);
        System.out.println(userbean.getNickname());
    }
}
