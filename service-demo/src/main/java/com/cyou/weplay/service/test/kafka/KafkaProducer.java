package com.cyou.weplay.service.test.kafka;

import java.util.Date;
import java.util.Properties;
import java.util.Random;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

/**
 * KafkaProducer
 *
 * @author zhaomingyu
 */
public class KafkaProducer {

    public static void main(String[] args) {
        Properties props = new Properties();

        props.put("metadata.broker.list", "10.127.131.22:9992");
        props.put("serializer.class", "kafka.serializer.StringEncoder");
        //props.put("partitioner.class", "example.producer.SimplePartitioner");
        //props.put("request.required.acks", "1");
        ProducerConfig config = new ProducerConfig(props);
        Producer<String, String> producer = new Producer<String, String>(config);
        long i = 1;
        while (true) {
            KeyedMessage<String, String> data = new KeyedMessage<String, String>("topic2", "k1", "" + i);
            producer.send(data);
            System.out.println(i);
            i++;
        }
    }
}
