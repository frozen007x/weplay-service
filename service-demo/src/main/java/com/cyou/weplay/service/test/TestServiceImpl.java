package com.cyou.weplay.service.test;

import com.cyou.weplay.cache.layer.UserHashLayer;
import com.cyou.weplay.cache.model.UserHash;
import com.cyou.weplay.service.BaseService;
import com.cyou.weplay.service.bean.UserBean;

/**
 * TestServiceImpl
 * @author zhaomingyu
 */
public class TestServiceImpl extends BaseService {

    public void testXA() throws Exception {
        UserHashLayer hashlayer = new UserHashLayer(tableShardManager);
        
        for (long i = 1; i <= 200; i++) {
            UserHash userhash = new UserHash(i);
            UserBean ub = userhash.getEntityBean();
            ub.setNickname("weplay" + i);
            ub.setStatus(0);
            ub.setUdid("udid" + i);
            ub.setImei("imei" + i);
            ub.setImsi("imsi" + i);
            ub.setSysid("sysid" + i);
            hashlayer.saveData(userhash);
        }

        throw new Exception("test");
    }
}
