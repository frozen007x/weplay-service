package com.cyou.weplay.service.test.xmpp;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

/**
 * TestSmack
 *
 * @author zhaomingyu
 */
public class TestSmack {

    public static void main(String[] args) throws Exception {
        /*
         ConnectionConfiguration config = new ConnectionConfiguration(
         "localhost", 5222);

         //config.setSASLAuthenticationEnabled(true);
         config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
         Connection conn = new XMPPConnection(config);
         try {
         conn.connect();

         conn.login("zmy", "zmy");
         } catch (XMPPException e) {
         e.printStackTrace();
         }*/

        
        ConnectionConfiguration config = new ConnectionConfiguration("127.0.0.1", 5222);
        XMPPConnection conn2 = new XMPPTCPConnection(config);
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        conn2.connect();
        
    }
}
