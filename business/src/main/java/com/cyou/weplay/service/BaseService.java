package com.cyou.weplay.service;

import com.cyou.weplay.db.TableShardManager;
import com.cyou.weplay.service.api.RemoteService;
import redis.clients.jedis.ShardedJedisPool;

/**
 * BaseService
 *
 * @author zhaomingyu
 */
public class BaseService implements RemoteService {

    protected TableShardManager tableShardManager = null;

    protected ShardedJedisPool jedisPool = null;

    public void setTableShardManager(TableShardManager tableShardManager) {
        this.tableShardManager = tableShardManager;
    }

    public void setJedisPool(ShardedJedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

}
