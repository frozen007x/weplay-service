package com.cyou.weplay.cache.model;

import com.cyou.weplay.cache.CacheKeys;

import java.lang.reflect.Field;
import java.util.Map;

import com.cyou.weplay.cache.layer.UserHashLayer;
import com.cyou.weplay.service.bean.UserBean;

/**
 * UserHash
 *
 * @author zhaomingyu
 */
public class UserHash extends RedisHash<UserBean> {

    private static final Map<String, Field> fieldMapping;

    private static final String mandatoryField;

    public static final String PREFIX = CacheKeys.USER_INFO;

    static {
        fieldMapping = RedisHash.makeFieldMapping(UserBean.class);
        if (fieldMapping != null && fieldMapping.containsKey(RedisHash.FIELDMAPPING_MANDATORY)) {
            mandatoryField = fieldMapping.get(RedisHash.FIELDMAPPING_MANDATORY).getName();
        } else {
            mandatoryField = null;
        }

    }

    @Override
    protected Map<String, Field> getFieldMapping() {
        return fieldMapping;
    }

    @Override
    protected String getMandatoryFieldName() {
        return mandatoryField;
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    public UserHash(long uid) {
        this.entityBean = new UserBean();
        this.entityBean.setUid(uid);
        this.entityId = String.valueOf(uid);
        this.expireSeconds = 7 * 24 * 3600; //默认7天有效期
    }

    public UserHash(long uid, UserHashLayer datalayer) {
        this(uid);
        this.datalayer = datalayer;
    }

    public UserHash(UserBean user) {
        this.entityBean = user;
        this.entityId = String.valueOf(this.entityBean.getUid());
    }
}
