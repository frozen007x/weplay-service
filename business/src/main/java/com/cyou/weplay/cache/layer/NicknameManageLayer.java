package com.cyou.weplay.cache.layer;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.cyou.weplay.db.ShardedJdbcDataLayer;
import com.cyou.weplay.db.TableShardInfo;
import com.cyou.weplay.db.TableShardManager;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * NicknameManageLayer
 *
 * @author zhaomingyu
 */
public class NicknameManageLayer extends ShardedJdbcDataLayer {
    private static final Logger logger = LoggerFactory.getLogger(NicknameManageLayer.class);

    private static final String ins_sql = "insert into %s (uid, nickname, name_md5) values(?,?,?)";

    public NicknameManageLayer(TableShardManager tableShardManager) {
        super(tableShardManager);
    }

    public void createNickname(long uid, String nickname) throws SQLException {
        String name_md5 = DigestUtils.md5Hex(nickname);
        TableShardInfo shardInfo = this.shardManager.getShardInfo("nickname_manage", name_md5);
        String tableName = shardInfo.getShardedName();
        if(logger.isDebugEnabled()) {
            logger.debug("save nickname " + nickname + " to " + tableName);
        }
        DataSource datasource = shardInfo.getDatasource();
        Connection connection = DataSourceUtils.getConnection(datasource);
        try {
            QueryRunner queryrunner = new QueryRunner();
            queryrunner.update(connection, String.format(ins_sql, tableName), uid, nickname, name_md5);
        } catch (SQLException ex) {
            throw ex;
        } finally {
            DataSourceUtils.releaseConnection(connection, datasource);
        }

    }
}
