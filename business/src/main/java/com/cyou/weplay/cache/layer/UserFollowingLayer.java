package com.cyou.weplay.cache.layer;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

import redis.clients.jedis.JedisCommands;

import com.cyou.weplay.db.ShardedJdbcDataLayer;
import com.cyou.weplay.db.TableShardInfo;
import com.cyou.weplay.db.TableShardManager;
import com.cyou.weplay.utils.SysTimeManager;

/**
 * UserFollowingLayer
 *
 * @author yangyu
 */
public class UserFollowingLayer extends ShardedJdbcDataLayer {
    private static final Logger logger = LoggerFactory.getLogger(UserFollowingLayer.class);

    private static final String ins_sql = "replace into %s (uid, following_uid, status, modifytime) values(?,?,?,current_timestamp())";

    public UserFollowingLayer(TableShardManager tableShardManager) {
        super(tableShardManager);
    }

    public int saveUserFollowing(long uid, long following_uid, int status) throws SQLException {
        int change = 0;
        String name_md5 = DigestUtils.md5Hex(uid + "" + following_uid);
        TableShardInfo shardInfo = this.shardManager.getShardInfo("user_following", name_md5);
        String tableName = shardInfo.getShardedName();
        if(logger.isDebugEnabled()) {
            logger.debug("save ["+ uid+ (status==0?" unfocus ":" focus ") + following_uid + "] to " + tableName);
        }
        DataSource datasource = shardInfo.getDatasource();
        Connection connection = DataSourceUtils.getConnection(datasource);
        try {
            QueryRunner queryrunner = new QueryRunner();
            change = queryrunner.update(connection, String.format(ins_sql, tableName), uid, following_uid, status);
        } catch (SQLException ex) {
            throw ex;
        } finally {
            DataSourceUtils.releaseConnection(connection, datasource);
        }
        return change;
    }
    
    public boolean isFocusUser(long myuid, long otheruid, JedisCommands jedis){
        String key = "py:focus:zset:" + myuid;
        Long cursor = null;
        boolean exi = jedis.exists(key);
        if (exi) {
            cursor = jedis.zrank(key, otheruid + "");
        } else {
            // this.getUserAllFocusIdList(myuid); 调用美玲姐滴方法
            cursor = jedis.zrank(key, otheruid + "");
        }
        if (cursor != null) {
            return true;
        }
        return false;
    }
    
    public void saveUserFollowingJedis(long uid, long following_uid, int status, JedisCommands jedis){
        String focuskey = "py:focus:zset:" + uid;
        if (status == 0) {
            jedis.zrem(focuskey, "" + following_uid);
        } else if (status == 1) {
            long timestamp = SysTimeManager.currentTimeMillis();
            jedis.zadd(focuskey, Double.valueOf(timestamp), "" + following_uid);
        }
    }
}
