package com.cyou.weplay.cache.layer;

import com.cyou.weplay.db.ShardedJdbcDataLayer;
import com.cyou.weplay.db.TableShardInfo;
import com.cyou.weplay.db.TableShardManager;
import com.cyou.weplay.service.bean.Usertoken;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * UsertokenLayer
 * @author zhaomingyu
 */
public class UsertokenLayer extends ShardedJdbcDataLayer {
    private static final Logger logger = LoggerFactory.getLogger(UsertokenLayer.class);

    private static final String get_by_udid = "select uid,udid,utoken from %s where udid=?";
    private static final String ins_sql = "insert into %s (uid, udid, utoken) values(?,?,?)";

    public UsertokenLayer(TableShardManager shardManager) {
        super(shardManager);
    }

    public Usertoken getUsertoken(String udid) throws SQLException {
        String udid_md5 = DigestUtils.md5Hex(udid);
        TableShardInfo shardInfo = this.shardManager.getShardInfo("user_token", udid_md5);
        String tableName = shardInfo.getShardedName();
        if(logger.isDebugEnabled()) {
            logger.debug("get token from " + tableName);
        }
        DataSource datasource = shardInfo.getDatasource();
        Connection connection = DataSourceUtils.getConnection(datasource);
        Usertoken usertoken;
        try {
            QueryRunner queryrunner = new QueryRunner();
            usertoken = queryrunner.query(connection, String.format(get_by_udid, tableName), new BeanHandler<Usertoken>(Usertoken.class), udid);
        } catch (SQLException ex) {
            throw ex;
        } finally {
            DataSourceUtils.releaseConnection(connection, datasource);
        }
        return usertoken;
    }

    public void createUsertoken(long uid, String udid, String token) throws SQLException {
        String udid_md5 = DigestUtils.md5Hex(udid);
        TableShardInfo shardInfo = this.shardManager.getShardInfo("user_token", udid_md5);
        String tableName = shardInfo.getShardedName();
        if(logger.isDebugEnabled()) {
            logger.debug("save uid " + uid + " token to " + tableName);
        }
        DataSource datasource = shardInfo.getDatasource();
        Connection connection = DataSourceUtils.getConnection(datasource);
        try {
            QueryRunner queryrunner = new QueryRunner();
            queryrunner.update(connection, String.format(ins_sql, tableName), uid, udid, token);
        } catch (SQLException ex) {
            throw ex;
        } finally {
            DataSourceUtils.releaseConnection(connection, datasource);
        }
    }

}
