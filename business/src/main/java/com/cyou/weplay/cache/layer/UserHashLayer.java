package com.cyou.weplay.cache.layer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import com.cyou.weplay.cache.model.UserHash;
import com.cyou.weplay.db.StringMapHandler;
import com.cyou.weplay.db.TableShardManager;
import com.cyou.weplay.redis.HashDataLayer;
import com.cyou.weplay.service.bean.UserBean;
import com.cyou.weplay.utils.DateUtils;
import com.cyou.weplay.utils.NumUtils;
import com.cyou.weplay.utils.SysTimeManager;

import org.apache.commons.dbutils.QueryRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UserHashLoader
 *
 * @author zhaomingyu
 */
public class UserHashLayer extends HashDataLayer<UserHash> {

    private static final Logger logger = LoggerFactory.getLogger(UserHashLayer.class);

    private static final String sel_sql = "select concat(uid) uid, nickname, avatar, avatar_m, avatar_s, avatar_bg, "
            + "concat(gender) gender, signature, areaid, favgame, concat(status) status, udid, imei, imsi, sysid, "
            + "unix_timestamp(createdate) createdate, DATE_FORMAT(birthdate,'%%Y-%%m-%%d') birthdate from %s where uid=?";

    private static final String ins_sql = "insert into %s (uid, nickname, status, udid, imei, imsi, sysid, createdate) values(?,?,?,?,?,?,?,?)";

    public UserHashLayer(TableShardManager shardManager) {
        super(shardManager);
        this.tableName = "weplay_user";
    }

    @Override
    protected Map<String, String> getData(String tableName, String id, Connection conn) throws SQLException {
        QueryRunner queryrunner = new QueryRunner();
        Map<String, String> map = queryrunner.query(conn, String.format(sel_sql, tableName), new StringMapHandler(), NumUtils.parseLong(id, 0));
        return map;
    }

    @Override
    protected void saveData(String tableName, UserHash e, Connection conn) throws SQLException {
        QueryRunner queryrunner = new QueryRunner();
        UserBean userbean = e.getEntityBean();
        if (logger.isDebugEnabled()) {
            logger.debug("save " + userbean.getUid() + " to " + tableName);
        }
        long currentTimeMillis = SysTimeManager.currentTimeMillis();
        userbean.setCreatedate(currentTimeMillis/1000);
        queryrunner.update(conn, String.format(ins_sql, tableName), userbean.getUid(), userbean.getNickname(), userbean.getStatus(), userbean.getUdid(), userbean.getImei(), userbean.getImsi(), userbean.getSysid(), DateUtils.getDateTime(currentTimeMillis));
    }

}
