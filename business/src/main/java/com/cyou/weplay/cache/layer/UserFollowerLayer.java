package com.cyou.weplay.cache.layer;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

import redis.clients.jedis.JedisCommands;

import com.cyou.weplay.db.ShardedJdbcDataLayer;
import com.cyou.weplay.db.TableShardInfo;
import com.cyou.weplay.db.TableShardManager;
import com.cyou.weplay.utils.SysTimeManager;

/**
 * UserFollowingLayer
 *
 * @author yangyu
 */
public class UserFollowerLayer extends ShardedJdbcDataLayer {
    private static final Logger logger = LoggerFactory.getLogger(UserFollowerLayer.class);

    private static final String ins_sql = "replace into %s (uid, follower_uid, status, modifytime) values(?,?,?,current_timestamp())";

    public UserFollowerLayer(TableShardManager tableShardManager) {
        super(tableShardManager);
    }

    public int saveUserFollower(long uid, long follower_uid, int status) throws SQLException {
        int change = 0;
        String name_md5 = DigestUtils.md5Hex(uid + "" + follower_uid);
        TableShardInfo shardInfo = this.shardManager.getShardInfo("user_follower", name_md5);
        String tableName = shardInfo.getShardedName();
        if(logger.isDebugEnabled()) {
            logger.debug("save ["+ uid+ (status==0?" unfans ":" fans ") + follower_uid + "] to " + tableName);
        }
        DataSource datasource = shardInfo.getDatasource();
        Connection connection = DataSourceUtils.getConnection(datasource);
        try {
            QueryRunner queryrunner = new QueryRunner();
            change = queryrunner.update(connection, String.format(ins_sql, tableName), uid, follower_uid, status);
        } catch (SQLException ex) {
            throw ex;
        } finally {
            DataSourceUtils.releaseConnection(connection, datasource);
        }
        return change;
    }
    
    public boolean isFansUser(long myuid, long otheruid, JedisCommands jedis) {
        String key = "py:fans:zset:" + myuid;
        Long cursor = null;
        boolean exi = jedis.exists(key);
        if (exi) {
            cursor = jedis.zrank(key, otheruid + "");
        } else {
            // this.getUserAllFansIdList(myuid); 调用美玲姐滴方法
            cursor = jedis.zrank(key, otheruid + "");
        }
        if (cursor != null) {
            return true;
        }
        return false;
    }
    
    public void saveUserFollowerJedis(long following_uid, long uid, int status, JedisCommands jedis){
        String fanskey = "py:fans:zset:" + following_uid;
        if (status == 0) {
            jedis.zrem(fanskey, "" + uid);
        } else if (status == 1) {
            long timestamp = SysTimeManager.currentTimeMillis();
            jedis.zadd(fanskey, Double.valueOf(timestamp), "" + uid);
        }
    }
}
