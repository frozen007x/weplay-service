package com.cyou.weplay.cache;

/**
 * CacheKeys
 *
 * @author zhaomingyu
 */
public class CacheKeys {

    public static final String ACC_UDID_UID = "weplay:acc:udid:uid:";

    public static final String USER_INFO = "weplay:user:info:hash:";

    public static final String SYSTEM_COUNTER = "weplay:sys:counter:hash:";
}
