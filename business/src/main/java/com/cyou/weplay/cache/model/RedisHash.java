package com.cyou.weplay.cache.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.cyou.weplay.redis.HashDataLayer;
import com.cyou.weplay.redis.RedisEntity;
import com.cyou.weplay.service.bean.FieldMapping;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisCommands;

/**
 * RedisHash
 *
 * @author zhaomingyu
 * @param <E>
 */
public abstract class RedisHash<E> extends RedisEntity {

    private static final Logger logger = LoggerFactory.getLogger(RedisHash.class);

    protected static final String FIELDMAPPING_MANDATORY = "field_mandatory";

    protected E entityBean;

    protected HashDataLayer<? extends RedisHash> datalayer = null;

    protected abstract Map<String, Field> getFieldMapping();

    protected abstract String getMandatoryFieldName();

    public E getEntityBean() {
        return entityBean;
    }

    public void getAllFields(JedisCommands jedis) throws Exception {
        getAllFields(getFieldMapping(), jedis);
    }

    public void getAllFields(Map<String, Field> fieldMapping, JedisCommands jedis) throws Exception {
        String key = getKey();
        Map<String, String> fields = jedis.hgetAll(key);

        if (fields == null || fields.isEmpty()) {
            fields = loadFromStorage();
            if (fields != null) {
                jedis.hmset(key, fields);
                this.resetExpire(key, jedis);
            }
        } else {
            String mFieldName = getMandatoryFieldName();
            if (mFieldName != null) {
                if (fields.get(mFieldName) == null) {
                    fields = loadFromStorage();
                    if (fields != null) {
                        jedis.hmset(key, fields);
                        this.resetExpire(key, jedis);
                    }
                }
            }
        }
        if (fields == null) {
            return;
        }

        Set<Entry<String, String>> fieldValueSet = fields.entrySet();
        for (Entry<String, String> fieldValue : fieldValueSet) {
            String name = fieldValue.getKey();
            String value = fieldValue.getValue();
            Field field = fieldMapping.get(name);
            if (field == null) {
                continue;
            }
            try {
                BeanUtils.setProperty(entityBean, field.getName(), value);
            } catch (IllegalAccessException e) {
                logger.error("getAllField error", e);
            } catch (InvocationTargetException e) {
                logger.error("getAllField error", e);
            }
        }
    }

    public void putAllFields(JedisCommands jedis) {
        putAllFields(entityBean, jedis);
    }

    public void putAllFields(E entityBean, JedisCommands jedis) {
        if (entityBean == null) {
            return;
        }
        Map<String, Field> fieldMapping = getFieldMapping();
        Map<String, String> entityBeanMap = new HashMap<String, String>();
        for (Entry<String, Field> entry : fieldMapping.entrySet()) {
            String value = null;
            try {
                value = BeanUtils.getProperty(entityBean, entry.getValue().getName());
            } catch (NoSuchMethodException e) {
                logger.error("putAllFields error", e);
            } catch (IllegalAccessException e) {
                logger.error("putAllFields error", e);
            } catch (InvocationTargetException e) {
                logger.error("putAllFields error", e);
            }
            entityBeanMap.put(entry.getKey(), value == null ? "" : value);
        }
        if (!entityBeanMap.isEmpty()) {
            jedis.hmset(getKey(), entityBeanMap);
        }
    }

    public void setHashDataLayer(HashDataLayer<? extends RedisHash> loader) {
        this.datalayer = loader;
    }

    protected Map<String, String> loadFromStorage() throws Exception {
        if (datalayer != null) {
            return datalayer.getData(entityId);
        }
        return null;
    }

    public static Map<String, Field> makeFieldMapping(Class clazz) {
        Map<String, Field> fieldMapping = new HashMap<String, Field>();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            FieldMapping fm = field.getAnnotation(FieldMapping.class);
            if (fm != null) {
                if (fm.name().equals("")) {
                    fieldMapping.put(field.getName(), field);
                } else {
                    fieldMapping.put(fm.name(), field);
                }
                if (fm.mandatory()) {
                    fieldMapping.put(FIELDMAPPING_MANDATORY, field);
                }
            }
        }
        return fieldMapping;
    }

    public String[] getFieldValues(JedisCommands jedis, String... fields) throws Exception {
        String key = getKey();
        if (!jedis.exists(key)) {
            RedisHash.this.getAllFields(jedis);
        }
        List<String> fieldList = jedis.hmget(key, fields);
        if (fieldList == null) {
            return null;
        }
        String[] fieldValues = new String[fields.length];
        fieldValues = fieldList.toArray(fieldValues);
        return fieldValues;
    }
}
