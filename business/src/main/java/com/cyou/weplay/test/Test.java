package com.cyou.weplay.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import com.cyou.weplay.cache.model.UserHash;
import com.cyou.weplay.db.StringMapHandler;
import com.cyou.weplay.redis.HashDataLayer;
import com.cyou.weplay.service.bean.UserBean;
import com.cyou.weplay.utils.DateUtils;
import org.apache.commons.dbutils.QueryRunner;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Test
 *
 * @author zhaomingyu
 */
public class Test {

    public static void main(String[] args) throws Exception {

        System.out.println(DateUtils.getCurrentDateTime());
    }
}
