package com.cyou.weplay.test;

import java.lang.reflect.Field;
import java.util.Map;

import com.cyou.weplay.cache.model.RedisHash;
import com.cyou.weplay.service.bean.UserBean;

/**
 * TestAnnotation
 * @author zhaomingyu
 */
public class TestAnnotation {

    public static void main(String[] args) {
        Map<String, Field> mapping = RedisHash.makeFieldMapping(UserBean.class);
        System.out.println(mapping.size());
    }
}
