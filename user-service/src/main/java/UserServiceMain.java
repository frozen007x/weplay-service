
import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * UserServiceMain
 *
 * @author zhaomingyu
 */
public class UserServiceMain {

    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"spring/applicationContext-data.xml", "spring/applicationContext-beans.xml", "spring/applicationContext-dubbo.xml", "spring/applicationContext-shard.xml"});
        context.start();
        System.out.println("user service started");
        System.in.read();
    }
}
