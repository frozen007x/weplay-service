package com.cyou.weplay.service.impl;

import com.cyou.weplay.cache.CacheKeys;
import com.cyou.weplay.cache.layer.UsertokenLayer;
import com.cyou.weplay.redis.JedisConnection;
import com.cyou.weplay.redis.JedisConnectionUtils;
import com.cyou.weplay.service.BaseService;
import com.cyou.weplay.service.api.AccountService;
import com.cyou.weplay.service.api.UserService;
import com.cyou.weplay.service.bean.UserBean;
import com.cyou.weplay.service.bean.Usertoken;
import com.cyou.weplay.service.exception.ServiceException;
import com.cyou.weplay.utils.NumUtils;
import com.cyou.weplay.utils.SecurityUtils;
import com.cyou.weplay.utils.StringUtils;

import org.apache.commons.codec.digest.DigestUtils;
import redis.clients.jedis.JedisCommands;

/**
 * AccountServiceImpl
 *
 * @author zhaomingyu
 */
public class AccountServiceImpl extends BaseService implements AccountService {

    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserBean matchUser(String secInfo) throws Exception {
        String udid = null;
        String imei = null;
        String imsi = null;
        String androidId = null;

        if (secInfo != null) {
            //加密的参数s获取设备号^SIM国际号^版本
            String[] info = SecurityUtils.decryptLoginInfo(secInfo);
            if (info == null || info.length < 3) {
                throw new ServiceException("006");
            }
            imei = info[0];//设备号
            imsi = info[1];//SIM国际号

            if (StringUtils.isNotEmpty(imsi) && !"null".equals(imsi)) {
                udid = DigestUtils.md5Hex(imei + "^" + imsi);
            } else if (StringUtils.isNotEmpty(imei) && !"null".equals(imei)) {
                udid = DigestUtils.md5Hex(imei);
            } else {
                if (info.length >= 4) {
                    androidId = info[3]; //AndroidId
                } else {
                    androidId = "null";
                }
                udid = DigestUtils.md5Hex(androidId);
            }
        } else {
            throw new ServiceException("006");
        }
        if (StringUtils.isEmpty(udid)) {
            throw new ServiceException("102");
        }

        long uid = 0;
        JedisConnection jedisConn = JedisConnectionUtils.getJedisConnection(jedisPool);
        JedisCommands jedis = jedisConn.getJedisCommands();
        //TODO: 需要考虑将udid生成规则由目前的MD5转为更安全的算法，防止碰撞
        String key = CacheKeys.ACC_UDID_UID + udid;
        String uidStr = jedis.get(key);
        if (uidStr == null) {
            //根据udid查询user_token表
            UsertokenLayer tokenLayer = new UsertokenLayer(tableShardManager);
            Usertoken usertoken = tokenLayer.getUsertoken(udid);
            if (usertoken != null) {
                uid = usertoken.getUid();
            }
            if (uid != 0) {
                jedis.set(key, String.valueOf(uid));
            }
        } else {
            uid = NumUtils.parseLong(uidStr, 0);
        }

        UserBean userbean;
        if (uid > 0) {
            jedis.expire(key, 7 * 24 * 3600); //重置过期时间为7天
            userbean = userService.getUserBean(uid);
        } else {
            userbean = userService.createUserBean(0, null, 0, udid, imei, imsi, androidId);
        }

        return userbean;
    }

    @Override
    public long authUser(String uauth, String token) throws Exception {
        if (StringUtils.isEmpty(uauth) || StringUtils.isEmpty(token)) {
            return 0;
        }

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String[] getUserAuth(long uid) throws Exception {
        return new String[]{"uauth", "token"};
    }
}
