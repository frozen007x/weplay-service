package com.cyou.weplay.service.impl;

import java.util.List;
import redis.clients.jedis.ShardedJedis;
import com.cyou.weplay.cache.layer.UserFollowerLayer;
import com.cyou.weplay.cache.layer.UserFollowingLayer;
import com.cyou.weplay.redis.JedisConnection;
import com.cyou.weplay.redis.JedisConnectionUtils;
import com.cyou.weplay.service.BaseService;
import com.cyou.weplay.service.api.RelationService;
import com.cyou.weplay.service.bean.UserBean;
import com.cyou.weplay.utils.SysTimeManager;

/**
 * RelationServiceImpl
 *
 * @author zhaomingyu
 */
public class RelationServiceImpl extends BaseService implements RelationService {
    
    @Override
    public void followUser(long uid, long followingUid) throws Exception {
        UserFollowingLayer focuslayer = new UserFollowingLayer(tableShardManager);
        UserFollowerLayer fanslayer = new UserFollowerLayer(tableShardManager);
        if (this.isFocusUser(uid, followingUid)) {
            return;
        }
        int change = focuslayer.saveUserFollowing(uid, followingUid, 1);
        if (change > 0) {
            fanslayer.saveUserFollower(followingUid, uid, 1);
            JedisConnection jedisConn = JedisConnectionUtils.getJedisConnection(jedisPool);
            ShardedJedis jedis = jedisConn.getJedis();
            focuslayer.saveUserFollowingJedis(uid, followingUid, 1, jedis);
            fanslayer.saveUserFollowerJedis(followingUid, uid, 1, jedis);
            JedisConnectionUtils.releaseJedisConnection(jedisConn, jedisPool);
        }

        if (this.isFansUser(uid, followingUid)) {
            long timestamp = SysTimeManager.currentTimeMillis();
            JedisConnection jedisConn = JedisConnectionUtils.getJedisConnection(jedisPool);
            ShardedJedis jedis = jedisConn.getJedis();
            String key = "py:friend:zset:";
            jedis.zadd(key + uid, Double.valueOf(timestamp), "" + followingUid);
            jedis.zadd(key + followingUid, Double.valueOf(timestamp), "" + uid);
            JedisConnectionUtils.releaseJedisConnection(jedisConn, jedisPool);
        }

        //this.pushRelation((int) uid, (int) otherUid,relationStatus);   看怎样处理
    }
    
    @Override
    public boolean isFocusUser(long myuid, long otheruid) {
        UserFollowingLayer layer = new UserFollowingLayer(tableShardManager);
        JedisConnection jedisConn = JedisConnectionUtils.getJedisConnection(jedisPool);
        ShardedJedis jedis = jedisConn.getJedis();
        boolean flag = layer.isFocusUser(myuid, otheruid, jedis);
        JedisConnectionUtils.releaseJedisConnection(jedisConn, jedisPool);
        return flag;
    }
    
    @Override
    public boolean isFansUser(long myuid, long otheruid) {
        UserFollowerLayer layer = new UserFollowerLayer(tableShardManager);
        JedisConnection jedisConn = JedisConnectionUtils.getJedisConnection(jedisPool);
        ShardedJedis jedis = jedisConn.getJedis();
        boolean flag = layer.isFansUser(myuid, otheruid, jedis);
        JedisConnectionUtils.releaseJedisConnection(jedisConn, jedisPool);
        return flag;
    }

    @Override
    public void unfollowUser(long uid, long followingUid) throws Exception{
        if (this.isFocusUser(uid, followingUid)) {
            UserFollowingLayer focuslayer = new UserFollowingLayer(tableShardManager);
            UserFollowerLayer fanslayer = new UserFollowerLayer(tableShardManager);
            int change = focuslayer.saveUserFollowing(uid, followingUid, 0);
            if (change > 0) {
                fanslayer.saveUserFollower(followingUid, uid, 0);
                JedisConnection jedisConn = JedisConnectionUtils.getJedisConnection(jedisPool);
                ShardedJedis jedis = jedisConn.getJedis();
                String key = "py:friend:zset:";
                jedis.zrem(key + uid, "" + followingUid);
                jedis.zrem(key + followingUid, "" + uid);
                focuslayer.saveUserFollowingJedis(uid, followingUid, 0, jedis);
                fanslayer.saveUserFollowerJedis(followingUid, uid, 0, jedis);
                JedisConnectionUtils.releaseJedisConnection(jedisConn, jedisPool);
            }
            
            /*
            try {
                MessageUtil.sendBrokeupMsg((int) uid, (int) otherUid, session);
            } catch (Exception e) {
                logger.error("sendBrokeupMsg error", e);
            }
            
            pushRelation((int) uid, (int) otherUid, 0);       
                                    看怎样处理
            */
        }
    }

    @Override
    public List<UserBean> getUserFollowings(long uid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UserBean> getUserFollowers(long uid) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
